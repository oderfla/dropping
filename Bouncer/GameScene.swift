//
//  GameScene.swift
//  Bouncer
//
//  Created by Haddad, Alfredo on 15/02/2016.
//  Copyright (c) 2016 Haddad, Alfredo. All rights reserved.
//

import SpriteKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


struct Constants {
    
    struct lines {
        
        static let kLineBlack = "line_black"
        static let kLineWhite = "line_white"
        static let kLineGrey  = "line_grey"
        static let kLineGreen = "line_green"
        static let kLineRed   = "line_red"
        static let kLineBrown = "line_brown"
        static let kLineOrange = "line_orange"
        static let kLineCream = "line_cream"
    }

    struct balls {
        
        static let kBallBlack = "ball_black"
        static let kBallWhite = "ball_white"
        static let kBallGrey  = "ball_grey"
        static let kBallGreen = "ball_green"
        static let kBallRed   = "ball_red"
        static let kBallBrown = "ball_brown"
        static let kBallOrange = "ball_orange"
        static let kBallCream = "ball_cream"
    }

    struct spikes {
        
        static let kSpikeBlack = "spike_black"
        static let kSpikeWhite = "spike_white"
        static let kSpikeGrey = "spike_grey"
        static let kSpikeGreen = "spike_green"
        static let kSpikeRed = "spike_red"
        static let kSpikeBrown = "spike_brown"
        static let kSpikeOrange = "spike_orange"
        static let kSpikeCream = "spike_cream"
    }

    struct backgrounds {

        static let kBg_1 = "bg_1"
        static let kBg_2 = "bg_2"
        static let kBg_3 = "bg_3"
        static let kBg_4 = "bg_4"
    }
    
    struct PhysicsCategory {
        
        static let None:         UInt32 = 0
        static let wallCategory: UInt32 = UInt32.max
        static let ballCategory: UInt32 = 0b1       // 1
        static let liveCategory: UInt32 = 0b10      // 2
        static let goldenCategory: UInt32 = 0b101
    }
    
    static let kDefault_bg = backgrounds.kBg_2
    static let kDefault_ball = balls.kBallOrange
    static let kDefault_line = lines.kLineBlack
    static let kDefault_spike = spikes.kSpikeRed
}

public var world: SKNode?
public var bg: SKSpriteNode?
public var ball: SKSpriteNode?
public var starting_line: SKSpriteNode?
public var lowest_node: SKSpriteNode?

public var lives: SKLabelNode?
public var score: SKLabelNode?

public var lastSpawnTimeInterval: TimeInterval?
public var lastUpdateTimeInterval: TimeInterval?

var global: Game?
var all_stages: Array<Stage>!
var currentStage: Stage!
var stage_position: Int!
var spikes_left: Int!
var lines_left: Int!
var duration: Double!
var ballExplosionFrames : [SKTexture]!

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    
    init(size: CGSize, stages:Array<Stage>) {
        
        super.init(size: size)
        all_stages = Array<Stage>()
        all_stages = stages
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        /* Setup your scene here */
        
        self.physicsWorld.contactDelegate = self
        
        stage_position = 0
        
        let explosionAnimationAtlas = SKTextureAtlas(named: "EXPLOSION")
        var explosionFrames = [SKTexture]()
        
        let numImages = explosionAnimationAtlas.textureNames.count
        
        for i in 1...numImages {
        
            let explosionTextureName = "\(i+5)"
            explosionFrames.append(explosionAnimationAtlas.textureNamed(explosionTextureName))
        }
        
        ballExplosionFrames = explosionFrames
        
        setStageProperties()
        setGameProperties()
        setWorldProperties()
        setGameStartingPoint()
    }
    
    func setStageProperties(){
    
        currentStage = Stage()
        currentStage = all_stages[stage_position]
        
        spikes_left = Int(currentStage.spikes)
        lines_left = Int(currentStage.duration)! - spikes_left - 1
        duration = Double(currentStage.speed)
    }
    
    func setGameProperties() {
    
        global = Game()
        global!.gameOver = false
        global!.gamePaused = false
        global!.contact_detection = false
        global!.stage_ended = false
        global!.gesture_allowed = true
        global!.total_lives = 3
        global!.total_score = 0
        global!.g_height = self.frame.size.height/100
        global!.g_width = self.frame.size.width/4
        lastUpdateTimeInterval = 0
        lastSpawnTimeInterval = 0
    }
    
    func setWorldProperties() {

        world = SKNode()
        world!.physicsBody = SKPhysicsBody(edgeLoopFrom: world!.frame)
        world!.physicsBody!.categoryBitMask = Constants.PhysicsCategory.wallCategory
        world!.physicsBody!.collisionBitMask = Constants.PhysicsCategory.ballCategory
        self.addChild(world!)
        
        // CREATING THE BACKGROUND
        
        bg = SKSpriteNode(imageNamed: Constants.backgrounds.kBg_4)
        bg!.size = CGSize(width: self.size.width, height: self.size.height)
        bg!.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        bg!.name = "background"
        bg!.zPosition = 0
        world!.addChild(bg!)
        
        //CREATING GESTURE
        
        let pan = UIPanGestureRecognizer(target: self, action:#selector(GameScene.handlePanGesture(_:)))
        self.view?.addGestureRecognizer(pan)
        
        // CREATING LIVES LABEL
        
        lives = SKLabelNode(fontNamed: "chalkduster")
        lives!.text = "Lives " + "\(global!.total_lives)"
        lives!.fontSize = 18
        lives!.fontColor = SKColor.black
        lives!.position = CGPoint(x: 40, y: self.size.height - 30)
        lives!.zPosition = 2
        self.addChild(lives!)
        
        // CREATING SCORE LABEL
        
        score = SKLabelNode(fontNamed: "chalkduster")
        score!.text = "Score: " + "\(global!.total_score)"
        score!.fontSize = 18
        score!.fontColor = SKColor.black
        score!.position = CGPoint(x: self.frame.size.width/2, y: self.size.height - 30)
        score!.zPosition = 2
        self.addChild(score!)
        
        // CREATE TOP BARRIER FOR COLLISION
        
        let topBarrier: SKSpriteNode = SKSpriteNode.init()
        topBarrier.name = "top"
        topBarrier.size = CGSize(width: self.frame.size.width, height: 2)
        topBarrier.physicsBody = SKPhysicsBody(rectangleOf: topBarrier.size)
        topBarrier.physicsBody!.isDynamic = false
        topBarrier.physicsBody!.categoryBitMask = Constants.PhysicsCategory.wallCategory
        topBarrier.physicsBody!.contactTestBitMask = Constants.PhysicsCategory.ballCategory
        topBarrier.zPosition = 1
        topBarrier.position = CGPoint(x: self.frame.midX, y: self.frame.size.height)
        world!.addChild(topBarrier)
        
        // CRETATE BOTTOM BARRIER FOR COLLISION
        
        let bottomBarrier: SKSpriteNode = SKSpriteNode.init()
        bottomBarrier.name = "bottom"
        bottomBarrier.size = CGSize(width: self.frame.size.width, height: 2)
        bottomBarrier.physicsBody = SKPhysicsBody(rectangleOf: bottomBarrier.size)
        bottomBarrier.physicsBody!.isDynamic = false
        bottomBarrier.physicsBody!.categoryBitMask = Constants.PhysicsCategory.wallCategory
        bottomBarrier.physicsBody!.contactTestBitMask = Constants.PhysicsCategory.ballCategory
        bottomBarrier.zPosition = 1
        bottomBarrier.position = CGPoint(x: self.frame.midX, y: 0)
        world!.addChild(bottomBarrier)
        
        //CREATE LEFT BARRIER FOR COLLISION
        
        let leftBarrier: SKSpriteNode = SKSpriteNode.init()
        leftBarrier.name = "left"
        leftBarrier.size = CGSize(width: 2, height: self.frame.size.height)
        leftBarrier.physicsBody = SKPhysicsBody(rectangleOf: leftBarrier.size)
        leftBarrier.physicsBody!.isDynamic = false
        leftBarrier.physicsBody!.categoryBitMask = Constants.PhysicsCategory.wallCategory
        leftBarrier.physicsBody!.contactTestBitMask = Constants.PhysicsCategory.ballCategory
        leftBarrier.zPosition = 1
        leftBarrier.position = CGPoint(x: 0, y: self.frame.midY)
        world!.addChild(leftBarrier)
        
        //CREATE RIGHT BARRIER FOR COLLISION

        let rightBarrier: SKSpriteNode = SKSpriteNode.init()
        rightBarrier.name = "right"
        rightBarrier.size = CGSize(width: 2, height: self.frame.size.height)
        rightBarrier.physicsBody = SKPhysicsBody(rectangleOf: rightBarrier.size)
        rightBarrier.physicsBody!.isDynamic = false
        rightBarrier.physicsBody!.categoryBitMask = Constants.PhysicsCategory.wallCategory
        rightBarrier.physicsBody!.contactTestBitMask = Constants.PhysicsCategory.ballCategory
        rightBarrier.zPosition = 1
        rightBarrier.position = CGPoint(x: self.frame.size.width, y: self.frame.midY)
        world!.addChild(rightBarrier)
    }
    
    func setGameStartingPoint(){
    
        starting_line = SKSpriteNode(imageNamed: Constants.kDefault_line)
        starting_line!.name = "main"
        starting_line!.size = CGSize(width: global!.g_width, height: global!.g_height)
        starting_line!.physicsBody = SKPhysicsBody(rectangleOf: starting_line!.size)
        starting_line!.physicsBody!.isDynamic = false
        starting_line!.physicsBody!.affectedByGravity = false
        starting_line!.zPosition = 1
        starting_line!.position = CGPoint(x: self.frame.midX, y: self.frame.minY + starting_line!.size.height)
        
        world!.addChild(starting_line!)
        
        ball = SKSpriteNode(imageNamed: Constants.kDefault_ball)
        ball!.name = "ball"
        ball!.physicsBody = SKPhysicsBody(circleOfRadius: ball!.size.width/2)
        ball!.position = CGPoint(x: self.frame.midX,y: self.frame.midY - self.frame.midY/2);
        ball!.xScale = 0.3
        ball!.yScale = 0.3
        ball!.physicsBody!.categoryBitMask = Constants.PhysicsCategory.ballCategory
        ball!.physicsBody!.collisionBitMask = Constants.PhysicsCategory.wallCategory
        ball!.physicsBody!.contactTestBitMask = Constants.PhysicsCategory.liveCategory
        ball!.zPosition = 1
        ball!.physicsBody!.allowsRotation = false
        
        self.addChild(ball!)

        let actionMove:SKAction = SKAction.move(to: CGPoint(x: self.frame.midX, y: self.frame.size.height + starting_line!.size.height/2), duration: Double(duration))
        let actionMoveDone:SKAction = SKAction.removeFromParent()
        let addToScore:SKAction = SKAction.run { () -> Void in
            global!.total_score = global!.total_score + 1
            score!.text = "Score: " + "\(global!.total_score)"
        }
        
        starting_line?.run(SKAction.sequence([actionMove, actionMoveDone, addToScore]))
    }
    
    func createLines() {
    
        let new_line: SKSpriteNode = SKSpriteNode(imageNamed: Constants.kDefault_line)
        new_line.size = CGSize(width: global!.g_width, height: global!.g_height)
        new_line.name = "line"
        new_line.physicsBody = SKPhysicsBody(rectangleOf: new_line.size)
        new_line.physicsBody!.isDynamic = false
        new_line.physicsBody!.affectedByGravity = false
        new_line.zPosition = 1
        
        let minX:Int = Int(CGFloat(new_line.size.width)/CGFloat(2))
        let maxX:Int = Int(self.frame.size.width) - minX
        
        let rangeX:Int = maxX - minX
        
        let actualX:Int = Int(arc4random_uniform(UInt32(rangeX))) + minX
        
        new_line.position = CGPoint(x: CGFloat(actualX), y: -(new_line.size.height/2))
        world!.addChild(new_line)
        
        let random:Int = Int(arc4random_uniform(5))
        
        if (random == 0){
        
            let live:SKSpriteNode = SKSpriteNode(imageNamed: Constants.balls.kBallGreen)
            live.name = "live"
            live.physicsBody = SKPhysicsBody(circleOfRadius: live.frame.size.width/2)
            live.physicsBody!.isDynamic = true
            live.physicsBody!.affectedByGravity = false
            live.physicsBody!.allowsRotation = false
            live.physicsBody!.categoryBitMask = Constants.PhysicsCategory.liveCategory
            live.physicsBody!.collisionBitMask = Constants.PhysicsCategory.wallCategory
            live.physicsBody!.contactTestBitMask = Constants.PhysicsCategory.ballCategory
            live.zPosition = 1
            live.xScale = 0.25
            live.yScale = 0.25
            live.position = CGPoint(x: new_line.position.x, y: new_line.position.y + live.size.height/2);
            
            world!.addChild(live)
        }
        
        let actionMove:SKAction = SKAction.move(to: CGPoint(x: CGFloat(actualX), y: self.frame.size.height + new_line.size.height/2), duration: Double(duration))
        let actionMoveDone:SKAction = SKAction.removeFromParent()
        let addToScore:SKAction = SKAction.run { () -> Void in
            global!.total_score = global!.total_score + 1
            score!.text = "Score: " + "\(global!.total_score)"
        }
        
        new_line.run(SKAction.sequence([actionMove, actionMoveDone, addToScore]))
    }
    
    func createSpikes() {
    
        let new_spike: SKSpriteNode = SKSpriteNode(imageNamed: Constants.kDefault_spike)
        new_spike.size = CGSize(width: global!.g_width, height: global!.g_height+3)
        new_spike.name = "spike"
        new_spike.physicsBody = SKPhysicsBody(rectangleOf: new_spike.size)
        new_spike.physicsBody!.isDynamic = false
        new_spike.physicsBody!.affectedByGravity = false
        new_spike.zPosition = 1
        new_spike.physicsBody!.categoryBitMask = Constants.PhysicsCategory.wallCategory
        new_spike.physicsBody!.contactTestBitMask = Constants.PhysicsCategory.ballCategory
        
        
        let minX:Int = Int(CGFloat(new_spike.size.width)/CGFloat(2))
        let maxX:Int = Int(self.frame.size.width) - minX
        
        let rangeX:Int = maxX - minX
        
        let actualX:Int = Int(arc4random_uniform(UInt32(rangeX))) + minX
        
        new_spike.position = CGPoint(x: CGFloat(actualX), y: -(new_spike.size.height/2))
        world!.addChild(new_spike)
        
        let actionMove:SKAction = SKAction.move(to: CGPoint(x: CGFloat(actualX), y: self.frame.size.height + new_spike.size.height/2), duration: Double(duration))
        let actionMoveDone:SKAction = SKAction.removeFromParent()
        let addToScore:SKAction = SKAction.run { () -> Void in
            global!.total_score = global!.total_score + 1
            score!.text = "Score: " + "\(global!.total_score)"
        }

        new_spike.run(SKAction.sequence([actionMove, actionMoveDone, addToScore]))
    }
    
    func handlePanGesture(_ recognizer:UIPanGestureRecognizer){
    
        if (world!.speed > 0) {
        
            if (recognizer.state == UIGestureRecognizerState.changed){
            
                var translation:CGPoint = recognizer.translation(in: recognizer.view)
                translation = CGPoint(x: translation.x, y: -translation.y)
                
                panForTranslation(translation)
                recognizer.setTranslation(CGPoint.zero, in: recognizer.view)
            }
        }
    }
    
    func panForTranslation(_ translation:CGPoint){
        
        if (global!.gesture_allowed){
        
            let position:CGPoint = ball!.position
            
            if (position.x + translation.x <= self.frame.size.width && position.x + translation.x >= 0){
                
                ball?.position = CGPoint(x: position.x + translation.x, y: position.y)
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        /* Called before each frame is rendered */
        
        if (!global!.gameOver && !global!.stage_ended) {
        
            var timeSinceLast:CFTimeInterval = currentTime - lastUpdateTimeInterval!
            lastUpdateTimeInterval = currentTime
            
            if (timeSinceLast > 1) {
                timeSinceLast = 1.0/60.0
                lastUpdateTimeInterval = currentTime
            }
            
            updateWithTimeSinceLastUpdate(timeSinceLast)
        }
    }
    
    func updateWithTimeSinceLastUpdate(_ timeSinceLast: CFTimeInterval){
    
        lastSpawnTimeInterval? += timeSinceLast
        
        if (lastSpawnTimeInterval > 1) {
            lastSpawnTimeInterval = 0
            
            let random:Int = Int(arc4random_uniform(5))
            
            if (lines_left + spikes_left > 0) {
            
                if (random % 2 == 0) {
                    
                    if (spikes_left > 0) {
                        
                        spikes_left = spikes_left - 1
                        createSpikes()
                    }
                    else{
                        
                        lines_left = lines_left - 1
                        createLines()
                    }
                }
                else{
                    
                    if (lines_left > 0){
                        lines_left = lines_left - 1
                        createLines()
                    }
                    else{
                        spikes_left = spikes_left - 1
                        createSpikes()
                    }
                }
            }
            else{
                // show golden line to end the stage
                showGoldenLine()
            }
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {

        if ((contact.bodyA.node?.name == "top" || contact.bodyA.node?.name == "bottom" || contact.bodyA.node?.name == "spike") && contact.bodyB.node?.name == "ball"){
            
            ball!.physicsBody!.velocity = CGVector(dx: 0, dy: 0)
            
            if (global!.total_lives > 1){
            
                global!.total_lives -= 1;
                lives!.text = "Lives " + "\(global!.total_lives)"
                
                ball!.removeFromParent()

                animateExplosion(contact)
            }
            else{
                //gameover
                
                global!.gameOver = true
                
                let loseAction = SKAction.run() {
                    let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
                    let gameOverScene = GameOver(size: self.size, score: global!.total_score, stages:all_stages)
                    self.view?.presentScene(gameOverScene, transition: reveal)
                }
                
                self.run(loseAction)
            }
        }
        else if ((contact.bodyA.node!.name == "top" && contact.bodyB.node!.name == "live") || (contact.bodyB.node!.name == "top" && contact.bodyA.node!.name == "live")) {
            
            if (contact.bodyA.node!.name == "live"){
                contact.bodyA.node!.removeFromParent()
            }
            else{
                contact.bodyB.node!.removeFromParent()
            }
        }
        else if ((contact.bodyA.node!.name == "ball" && contact.bodyB.node!.name == "live") || (contact.bodyB.node!.name == "live" && contact.bodyA.node!.name == "ball")){
            
            global!.total_lives += 1
            lives!.text = "Lives " + "\(global!.total_lives)"
            
            if (contact.bodyA.node!.name == "live"){
                contact.bodyA.node!.removeFromParent()
            }
            else{
                contact.bodyB.node!.removeFromParent()
            }
        }
        else if ((contact.bodyA.node!.name == "ball" && contact.bodyB.node!.name == "golden") || (contact.bodyB.node!.name == "ball" && contact.bodyA.node!.name == "golden")){
        
            global!.gesture_allowed = false
        }
    }
    
    func animateExplosion(_ contact:SKPhysicsContact) {
        
        for node in world!.children as! [SKSpriteNode] {
            
            if (node.name == "line" || node.name == "main" || node.name == "golden"){
                
                if (lowest_node != nil) {
                    
                    if (node.position.y < lowest_node?.position.y){
                        
                        lowest_node = node
                    }
                }
                else{
                    lowest_node = node
                }
            }
        }
        
        let explosion:SKSpriteNode = SKSpriteNode(texture: ballExplosionFrames[0])
        explosion.zPosition = 1
        explosion.setScale(0.5)
        explosion.position = contact.bodyB.node!.position

        world!.addChild(explosion)

        let explosionAction:SKAction = SKAction.animate(with: ballExplosionFrames, timePerFrame: 0.05)
        let remove:SKAction = SKAction.removeFromParent()
        let relocate:SKAction = SKAction.run { () -> Void in
            self.relocateBall()
        }
        
        explosion.run(SKAction.sequence([explosionAction, remove, relocate]))
    }
    
    func relocateBall() {
        
        ball = SKSpriteNode(imageNamed: Constants.kDefault_ball)
        ball!.name = "ball"
        ball!.physicsBody = SKPhysicsBody(circleOfRadius: ball!.size.width/2)
        ball!.position = CGPoint(x: lowest_node!.position.x, y: lowest_node!.position.y + ball!.size.height/2);
        ball!.xScale = 0.3
        ball!.yScale = 0.3
        ball!.physicsBody!.categoryBitMask = Constants.PhysicsCategory.ballCategory
        ball!.physicsBody!.collisionBitMask = Constants.PhysicsCategory.wallCategory
        ball!.physicsBody!.contactTestBitMask = Constants.PhysicsCategory.liveCategory
        ball!.zPosition = 1
        ball!.physicsBody!.allowsRotation = false
        
        if (lowest_node == nil){
        
            lowest_node = SKSpriteNode(imageNamed: Constants.kDefault_line)
            lowest_node!.name = "main"
            lowest_node!.size = CGSize(width: global!.g_width, height: global!.g_height)
            lowest_node!.physicsBody = SKPhysicsBody(rectangleOf: lowest_node!.size)
            lowest_node!.physicsBody!.isDynamic = false
            lowest_node!.physicsBody!.affectedByGravity = false
            lowest_node!.zPosition = 1
            lowest_node!.position = CGPoint(x: self.frame.midX, y: self.frame.minY + lowest_node!.size.height)
            
            world!.addChild(lowest_node!)
            self.addChild(ball!)
            
            let actionMove:SKAction = SKAction.move(to: CGPoint(x: self.frame.midX, y: self.frame.size.height + lowest_node!.size.height/2), duration: Double(duration/2))
            let actionMoveDone:SKAction = SKAction.removeFromParent()
            let addToScore:SKAction = SKAction.run { () -> Void in
                global!.total_score = global!.total_score + 1
                score!.text = "Score: " + "\(global!.total_score)"
            }
            
            lowest_node?.run(SKAction.sequence([actionMove, actionMoveDone, addToScore]))
        }
        else{
            self.addChild(ball!)
        }
    }
    
    func showGoldenLine(){
    
        global!.stage_ended = true
        
        let golden_line = SKSpriteNode(imageNamed: Constants.lines.kLineBrown)
        golden_line.name = "golden"
        golden_line.size = CGSize(width: global!.g_width, height: global!.g_height)
        golden_line.physicsBody = SKPhysicsBody(rectangleOf: golden_line.size)
        golden_line.physicsBody!.isDynamic = false
        golden_line.physicsBody!.affectedByGravity = false
        golden_line.physicsBody!.categoryBitMask = Constants.PhysicsCategory.goldenCategory
        golden_line.physicsBody!.collisionBitMask = Constants.PhysicsCategory.wallCategory
        golden_line.physicsBody!.contactTestBitMask = Constants.PhysicsCategory.ballCategory
        golden_line.zPosition = 1
        golden_line.position = CGPoint(x: self.frame.midX, y: -(golden_line.size.height/2))
        
        world!.addChild(golden_line)
        
        let test:Double = Double(duration)/2
        
        let actionMove:SKAction = SKAction.move(to: CGPoint(x: self.frame.midX,y: self.frame.midY), duration: Double(test))
        let actionDelay:SKAction = SKAction.wait(forDuration: test)
        let showPopup:SKAction = SKAction.run { () -> Void in
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "popup") as! PopupViewController
            vc.stage = stage_position + 1
            vc.view.tag = -1
            vc.view.frame = CGRect(x: 0, y: -200, width: 200, height: 200)
            vc.view.alpha = 0.0
            self.view!.addSubview(vc.view)
            
            UIView.animate(withDuration: 1, animations: { () -> Void in
                
                vc.view.center = CGPoint(x: self.frame.midX, y: self.frame.midY - self.frame.height/8)
                vc.view.alpha = 1.0
            })
        }
        let popupDelay:SKAction = SKAction.wait(forDuration: 6)
        let removePopup:SKAction = SKAction.run { () -> Void in
            
            self.view!.viewWithTag(-1)!.removeFromSuperview()
            stage_position = stage_position + 1
            ball!.removeFromParent()
            golden_line.removeFromParent()
            self.setStageProperties()
            self.setGameStartingPoint()
            global!.stage_ended = false
            global!.gesture_allowed = true
        }

        
        golden_line.run(SKAction.sequence([actionMove, actionDelay, showPopup, popupDelay, removePopup]))
    }
}
