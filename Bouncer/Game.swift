//
//  Game.swift
//  Bouncer
//
//  Created by Haddad, Alfredo on 16/02/2016.
//  Copyright © 2016 Haddad, Alfredo. All rights reserved.
//

import UIKit

class Game: NSObject {

    var gameOver: Bool = false
    var gamePaused: Bool = false
    var contact_detection: Bool = false
    var stage_ended: Bool = false
    var gesture_allowed: Bool = true
    
    var total_lives: Int = 3
    var total_score: Int = 0
    
    var g_width: CGFloat = 0
    var g_height: CGFloat = 0
    
    
    override init() {
        
    }
}