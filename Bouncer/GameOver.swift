//
//  GameOver.swift
//  Bouncer
//
//  Created by Haddad, Alfredo on 17/02/2016.
//  Copyright © 2016 Haddad, Alfredo. All rights reserved.
//

import Foundation
import SpriteKit

class GameOver: SKScene {

    init(size: CGSize, score:Int, stages:Array<Stage>) {
        
        super.init(size: size)
        
        self.backgroundColor = SKColor(colorLiteralRed: 1.0, green: 1.0, blue: 1.0, alpha: 1)

        if (Util().checkScore(score)){
        
            let high_score:SKLabelNode = SKLabelNode(fontNamed: "chalkduster")
            high_score.text = "New High Score!"
            high_score.fontSize = 30
            high_score.fontColor = SKColor.black
            high_score.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
            self.addChild(high_score)
            
            let actual_score:SKLabelNode = SKLabelNode(fontNamed: "chalkduster")
            actual_score.text = "\(score)"
            actual_score.fontSize = 40
            actual_score.fontColor = SKColor.black
            actual_score.position = CGPoint(x: self.size.width/2, y: (self.size.height/2) - 50)
            self.addChild(actual_score)
        }
        else{
        
            let message:String = "Score: " + "\(score)"
            
            let messageLabel:SKLabelNode = SKLabelNode(fontNamed:"chalkduster")
            messageLabel.text = message
            messageLabel.fontSize = 30
            messageLabel.fontColor = SKColor.black
            messageLabel.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
            self.addChild(messageLabel)
        }
        
        run(SKAction.sequence([SKAction.wait(forDuration: 3.0), SKAction.run() {
                let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
                let scene = GameScene(size: size, stages: stages)
                self.view?.presentScene(scene, transition:reveal)
            }
            ]))
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
