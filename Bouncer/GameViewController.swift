//
//  GameViewController.swift
//  Bouncer
//
//  Created by Haddad, Alfredo on 15/02/2016.
//  Copyright (c) 2016 Haddad, Alfredo. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {

    @IBOutlet weak var icon: UIImageView!
    
    @IBOutlet weak var home_title: UIImageView!
    
    @IBOutlet weak var high_score_value: UILabel!
    
    @IBOutlet weak var high_score_image: UIImageView!
    
    @IBOutlet weak var play: UIButton!
    
    @IBOutlet weak var about: UIButton!
    
    @IBOutlet weak var menuContainer: UIView!
    
    var all_stages: Array<Stage>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        if (Util().getHighScore() == 0){
        
            self.high_score_value.text = String(0)
        }
        else{
        
            self.high_score_value.text = String(Util().getHighScore())
            self.high_score_value.isHidden = false
            self.high_score_image.isHidden = false
        }
        
        parseStages()
    }

    override var shouldAutorotate : Bool {
        return true
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    func parseStages() {

        all_stages = Array<Stage>()

        let path: NSString = Bundle.main.path(forResource: "stages", ofType: "json")! as NSString
        let data : Data = try! Data(contentsOf: URL(fileURLWithPath: path as String), options: NSData.ReadingOptions.dataReadingMapped)
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            
            if let dic = json as? NSDictionary{
                
                if let stages = dic["stages"] as? [[String: AnyObject]] {
                    
                    for stage in stages {
                        
                        let aux:Stage = Stage()
                        aux.id = stage["id"] as! String
                        aux.speed = stage["speed"] as! String
                        aux.duration = stage["duration"] as! String
                        aux.spikes = stage["spikes"] as! String
                        
                        all_stages.append(aux)
                    }
                }
            }
            
        } catch {
            print("error serializing JSON: \(error)")
        }
    }
    
    @IBAction func startGame(_ sender: UIButton) {
        
        self.menuContainer.isHidden = true
        
        let scene = GameScene(size:view.bounds.size, stages:all_stages)
        let skView = view as! SKView
        skView.showsFPS = true
        skView.showsPhysics = true
        skView.showsNodeCount = true
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .aspectFill
        skView.presentScene(scene)
    }
}
