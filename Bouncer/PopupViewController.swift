//
//  PopupViewController.swift
//  Bouncer
//
//  Created by Haddad, Alfredo on 25/02/2016.
//  Copyright © 2016 Haddad, Alfredo. All rights reserved.
//

import UIKit

class PopupViewController: UIViewController {

    var stage:Int!
    var count:Int!
    var timer:Timer!
    
    @IBOutlet weak var stage_label: UILabel!
    @IBOutlet weak var countdown_label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        count = 5
        stage_label!.font = UIFont(name: "chalkduster", size: 16)
        stage_label!.text = "STAGE " + "\(stage!)" + " CLEARED"
        
        countdown_label!.font = UIFont(name: "chalkduster", size: 70)
        countdown_label!.text = "\(count)"

        startCountDown()
    }

    func startCountDown() {
    
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(PopupViewController.update), userInfo: nil, repeats: true)
    }
    
    func update() {
        
        if(count > 0)
        {
            countdown_label.text = String(count)
            count = count - 1
        }
        else{
            timer.invalidate()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
