//
//  Util.swift
//  Bouncer
//
//  Created by Haddad, Alfredo on 11/04/2016.
//  Copyright © 2016 Haddad, Alfredo. All rights reserved.
//

import UIKit

class Util: NSObject {
    
    func checkScore(_ score:Int) -> Bool {
        
        let defaults = UserDefaults.standard
        
        let currentHighScore = defaults.integer(forKey: "high_score")
        
        if (currentHighScore == 0){
            
            if (score > currentHighScore) {
                
                defaults.set(score, forKey: "high_score")
                return true
            }
            return false
        }
        else{
            
            if (score > currentHighScore) {
                
                defaults.set(score, forKey: "high_score")
                return true
            }
        }
        
        return false
    }
    
    func getHighScore() -> Int {
        
        let defaults = UserDefaults.standard
        
        return defaults.integer(forKey: "high_score")
    }
}
